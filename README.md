# Patchagent #

[![build status](https://gitlab.com/ixilon/patchagent/badges/master/build.svg)](https://gitlab.com/ixilon/patchagent/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/patchagent/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/patchagent)

## Overview ##
Replace classes of an external Java library with an alternative implementation.

Sometimes you want to use an external Java library but with modifications of some of its classes.
If you can't rebuild the library after modification of it's source code, then this helper is your friend:
Simply write a replacement class and put it at the same package location within your JAR.

## Usage ##
* write replacement classes with same name and package location
* load agent at the begin of main()

```
import de.ixilon.PatchAgentLoader
    
public class Application {
  public static void main(String[] args) {
    PatchAgentLoader.loadAgent();
    // ...
  }
}
```

## Links ##
* [Patching Java at runtime](https://www.javacodegeeks.com/2012/02/patching-java-at-runtime.html)
* [Package java.lang.instrument](http://docs.oracle.com/javase/6/docs/api/java/lang/instrument/package-summary.html)
* [How can I add a Javaagent to a JVM without stopping the JVM?](http://stackoverflow.com/questions/4817670/how-can-i-add-a-javaagent-to-a-jvm-without-stopping-the-jvm)
* [EA Agent Loader](https://github.com/electronicarts/ea-agent-loader)
* [Howto get class bytecode](http://stackoverflow.com/questions/26637137/java-given-classloader-and-class-get-class-bytecode)
* [An introduction to Java Agent and bytecode manipulation](http://www.tomsquest.com/blog/2014/01/intro-java-agent-and-bytecode-manipulation/)
* [Publishing to the Maven Central using Gradle](http://nemerosa.ghost.io/2015/07/01/publishing-to-the-maven-central-using-gradle/)
