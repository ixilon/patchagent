package de.ixilon.instrument;

import com.ea.agentloader.AgentLoader;

public final class PatchAgentLoader {
  private PatchAgentLoader() {
  }
  
  public static void loadAgent() {
    AgentLoader.loadAgentClass(PatchAgent.class.getName(), "");
  }
}
