package de.ixilon.instrument;

import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.ProtectionDomain;
import java.util.Arrays;

import com.google.common.base.Joiner;

import sun.misc.IOUtils;

/**
 * This Java agent can be loaded with
 * 
 * PatchAgentLoader.loadAgent();
 * 
 * Local classes within this JAR will overwrite classes of external libraries
 * with same package and class name.
 */
public class PatchAgent implements ClassFileTransformer {

  private final ClassLoader classLoader;

  public PatchAgent() {
    String name = toPath(getClass().getName()) + ".class";
    String path = getClass().getResource(name).getPath();
    String prefix = "jar:" + path.substring(0, path.indexOf("!") + 2);
    final URL url;

    try {
      url = new URL(prefix);
    } catch (MalformedURLException exception) {
      throw new IllegalStateException(exception);
    }

    classLoader = new URLClassLoader(new URL[] { url });
  }

  private static String toPath(String name) {
    return "/" + Joiner.on("/").join(Arrays.asList(name.split("\\.")));
  }

  /**
   * Used when loaded with JVM parameter
   * -javaagent:de.ixilon.instrument.PatchAgent
   */
  public static void premain(String argument, Instrumentation instrumentation) {
    agentmain(argument, instrumentation);
  }

  /**
   * Used when loaded via META-INF or AgentLoader
   */
  public static void agentmain(String argument, Instrumentation instrumentation) {
    instrumentation.addTransformer(new PatchAgent());
  }

  /**
   * Try to load the class file within the current JAR. If its not found, then
   * null is returned and class loader will search this class in the classpath.
   */
  @Override
  public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
      ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
    String path = "/" + className + ".class";
    try (InputStream input = classLoader.getResourceAsStream(path)) {
      if (input != null) {
        return IOUtils.readFully(input, -1, true);
      }
    } catch (IOException exception) {
      throw new IllegalClassFormatException(path);
    }
    return null;
  }

}
